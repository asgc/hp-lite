module hp-client-golang

go 1.18

require (
	github.com/golang/protobuf v1.5.2
	github.com/olekukonko/tablewriter v0.0.5
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/mattn/go-runewidth v0.0.9 // indirect
	golang.org/x/mobile v0.0.0-20230531173138-3c911d8e3eda // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/sync v0.0.0-20220819030929-7fc1605a5dde // indirect
	golang.org/x/sys v0.0.0-20220722155257-8c9f86f7a55f // indirect
	golang.org/x/tools v0.1.12 // indirect
)
